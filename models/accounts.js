const Sequelize = require('sequelize');

// custom imports
const sequelize = require('../config/db');

const Accounts = sequelize.define('accounts', {
    username: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    }
})



module.exports = Accounts;