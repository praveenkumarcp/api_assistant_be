const bcrypt = require('bcrypt');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const cmd = require('node-cmd');

// custom imports
const AccountsDao = require('../dao/accounts');

const authenticate = async(mail, password)=>{
    var accountsDao = new AccountsDao();
    var user_obj = await accountsDao.getAccountByEmail(mail);
    if(user_obj == null)
        return null;
    else{
        var result = bcrypt.compareSync(password, user_obj.password);
        if(result == true)
            return user_obj;
        return null;
    }
}

const generateJWT = (user_obj)=>{
    var claim = {user_id: user_obj.id};
    var privateKey = fs.readFileSync('private.key','utf-8');
    var token = jwt.sign(claim, privateKey);
    return token;
}

const attachUserObject = async(request, response, next)=>{
    if(request.path !='/api/v1/accounts/signup' && request.path !='/api/v1/accounts/login'){
        var incoming_token = request.get('Authorization');
        var publicKey = fs.readFileSync('public.key','utf-8');
       try{
           var decoded =  jwt.decode(incoming_token, publicKey);
           var accountsDao = new AccountsDao();
           var user_obj = await accountsDao.getAccountByID(decoded.user_id);
           user_obj.port = user_obj.id + 5000;
           request.user = user_obj;
       }
       catch(err){
           console.log("error occured during decoding JWT", err);
       }
       
    }
    next();
}


module.exports.authenticate = authenticate;
module.exports.generateJWT = generateJWT;
module.exports.attachUserObject = attachUserObject;
