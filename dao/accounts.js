const bcrypt = require('bcrypt');

// custom imports
const Accounts = require('../models/accounts');

class AccountsDao{
    addAccount(data){
        this.username = data.username;
        this.password = bcrypt.hashSync(data.password, 10);
        this.email = data.email;
    }

    async saveAndReturn(){
        var data;
        await Accounts.create(this)
        .then(result => {
            data = result.dataValues;
        })
        .catch(err=>{
            data = err.errors[0];
        })
        return data;
    }

    async getAccountByEmail(email){
        var data;
        await Accounts.findOne({
            where:{
                email: email
            }
        })
        .then((result)=>{
            if(result == null)
                data = null;
            else
                data = result.dataValues;
        })
        .catch(err=>{
            data = err;
        });
        return data;
    }
    async getAccountByID(id){
        var data;
        await Accounts.findOne({
            where:{
                id: id
            }
        })
        .then(result=>{
            if(result == null)
                data = null;
            else    
                data = result.dataValues;
        })
        .catch(err=>{
            data = err;
        });
        return data;
    }

    
}

module.exports = AccountsDao;
