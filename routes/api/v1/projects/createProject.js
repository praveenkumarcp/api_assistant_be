const express = require('express');
const router = express.Router();
const cmd = require('node-cmd');
const fs = require('fs');

// custom imports
const Utils = require('../../../../utils/utils');
const RES = require('../../../../resources/response');


router.post('/', async(request, response)=>{
    cmd.get(
        `
        cd ../../
        cd api_pro/${request.user.email}
        ls
        `,function(err, data, stderr){
            var no_of_projects = data.split('\n').length - 1;
            if(no_of_projects == 5){
                response.send(RES.max_no_project);
            }
            else{
                try{
                cmd.run(
                    `
                    sudo -u praveen createdb ${request.body.folder_name+request.user.id};
                    cd ../../
                    cd api_pro/${request.user.email}
                    mkdir ${request.body.folder_name}
                    cd ${request.body.folder_name}
                    touch index.js
                    touch home.html
                    touch config.js
                    mkdir models
                    npm init -y
                    npm i express --save
                    npm i sequelize --save 
                    npm i pg pg-hstore --save
                `)
                }
                catch(err){
                    console.log("error creating", err);
                }
                // adding nodemon setup
                setTimeout(async()=>{
                    // reading package.json content
                    // var package_content = JSON.parse(fs.readFileSync(`/home/praveen/Documents/api_pro/${request.user.email}/${request.body.folder_name}/package.json`, 'utf-8'));
                    // package_content.scripts.start = "nodemon index.js";

                    // // writing to package.json file
                    // fs.writeFile(`/home/praveen/Documents/api_pro/${request.user.email}/${request.body.folder_name}/package.json`, JSON.stringify(package_content), function (err) {
                    //     if (err) throw err;
                    //     console.log('Updated package.json file !');
                    // })

                    // reading static index.js and home.html file
                    var static_index = fs.readFileSync('/home/praveen/Documents/api_static/index.js', 'utf-8');
                    var static_html = fs.readFileSync('/home/praveen/Documents/api_static/home.html', 'utf-8');
                    var config_content = fs.readFileSync('/home/praveen/Documents/api_static/config.js', 'utf-8');
                    config_content = config_content.replace('DB_NAME', '"'+request.body.folder_name+request.user.id+'"');
                    
                    // server port changing for user 
                    var port_string  = 'app.listen(' + `${request.user.port.toString()}` + ',()=>console.log("server started on port '+`${request.user.port.toString()}`+ '"))';

                    // writing to index.js file
                    await fs.writeFile(`/home/praveen/Documents/api_pro/${request.user.email}/${request.body.folder_name}/index.js`, static_index , function (err) {
                        if (err) throw err;
                        console.log('Updated index.js file!');
                    })

                    // writing port number
                    await fs.appendFile(`/home/praveen/Documents/api_pro/${request.user.email}/${request.body.folder_name}/index.js`, port_string , function (err) {
                        if (err) throw err;
                        console.log('Updated index.js port number!');
                    })

                    // writing to home.html
                    await fs.writeFile(`/home/praveen/Documents/api_pro/${request.user.email}/${request.body.folder_name}/home.html`, static_html, function (err) {
                        if (err) throw err;
                        console.log('Updated home.html file!');
                    })

                    // writing to config.js
                    await fs.writeFile(`/home/praveen/Documents/api_pro/${request.user.email}/${request.body.folder_name}/config.js`, config_content, function (err) {
                        if (err) throw err;
                        console.log('Updated config.js file!');
                    })
                }, 5000);

                response.send(RES.project_created);               
            }
        }
    )
})

module.exports = router;