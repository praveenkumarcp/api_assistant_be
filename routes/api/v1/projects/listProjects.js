const express = require('express');
const router = express.Router();
const cmd = require('node-cmd');

// custom imports
const RES = require('../../../../resources/response');

router.get('/', (request, response)=>{
    cmd.get(
        `
        cd ../../
        cd api_pro/${request.user.email}
        ls
        `, function(err, data, stderr){
            var resp_obj = RES.projects_fetced;
            var project_list = data.split('\n');
            resp_obj.projects = project_list.splice(0, project_list.length-1);
            response.send(resp_obj);
        }
    )
});

module.exports = router;