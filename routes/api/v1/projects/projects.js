const express = require('express');
const router = express.Router();

// custom imports
const createProject = require('./createProject');
const listProjects = require('./listProjects');
const server = require('./server');

router.use('/projects', [createProject, listProjects, server]);

module.exports = router;