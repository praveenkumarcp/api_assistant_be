const express = require('express');
const router = express.Router();
const cmd = require('node-cmd');

router.get('/:pname/server', (request, response)=>{
    console.log("server starting");
    cmd.get(
        `
        cd ../../
        cd api_pro/${request.user.email}/${request.params.pname}/
        node index.js
        `, function(err, data, stderr){
            console.log(data);
        }
    )
    var resp_obj = {"status":"server started","port":request.user.port};
    response.send(resp_obj);

})

router.post('/:pname/server', (request, response)=>{
    console.log("stopping server");
    cmd.get(
        `kill $(lsof -t -i:${request.user.port})`,
        function(err, data, stderr){
            console.log(data);
        }
    )
    var resp_obj = {"status":"server stopped"};
    response.send(resp_obj);

})

module.exports = router;
