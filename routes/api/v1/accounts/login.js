const express = require('express');
const router = express.Router();

// custom imports
const Utils = require('../../../../utils/utils');
const RES = require('../../../../resources/response');

router.post('/', async(request, response)=>{
    var result = await Utils.authenticate(request.body.email, request.body.password);
    var resp_obj;
    if(result == null){
        resp_obj = RES.invalid_login;
        response.status(200).send(resp_obj);
    }
    else{
        resp_obj = RES.valid_login;
        resp_obj.token = Utils.generateJWT(result);
        resp_obj.username = result.username;
        response.status(201).send(resp_obj);
    }
})

module.exports = router;