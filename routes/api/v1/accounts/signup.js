const express = require('express');
const router = express.Router();
const cmd = require('node-cmd');

// custom imports
const AccountsDao = require('../../../../dao/accounts');
const RES = require('../../../../resources/response');
const Utils = require('../../../../utils/utils');
const GATEKEEPER = require('../../../../engineering/gatekeeper');

router.post('/', async(request, response)=>{
    var accountsDao = new AccountsDao();
    accountsDao.addAccount(request.body);
    var data = await accountsDao.saveAndReturn();
    var resp_obj;
    if(data.message){
        resp_obj = RES.account_not_created;
        resp_obj.error = data.message;
        GATEKEEPER(response, resp_obj, 200);
    }
    else{
            // creating project root folder for user
            cmd.run(
                `
                cd ../../
                cd api_pro
                mkdir ${data.email}
                `
            );
        resp_obj = RES.account_created;
        resp_obj.token = Utils.generateJWT(data);
        resp_obj.account = data;
        GATEKEEPER(response, resp_obj, 201);
    }
})

module.exports = router;
