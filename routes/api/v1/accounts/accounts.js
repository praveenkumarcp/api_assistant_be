const express = require('express');
const router = express.Router();

// custom imports
const signup = require('./signup');
const login = require('./login');

router.use('/signup', signup);
router.use('/login', login);

module.exports = router;