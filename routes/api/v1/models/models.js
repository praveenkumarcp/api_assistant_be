const express = require('express');
const router = express.Router();

// custom imports
const createModel = require('./createModel');
const getModels = require('./getModels');
const specificModel = require('./specifciModel');
const editModel = require('./editModel');

const attachProjectName = (request, response, next)=>{
    request.body.pname = request.params.pname;
    next();
}

router.use('/:pname/models', [attachProjectName, createModel, getModels, specificModel, editModel]);


module.exports = router;