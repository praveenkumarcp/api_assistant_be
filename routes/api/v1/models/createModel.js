const express = require('express');
const router = express.Router();
const cmd = require('node-cmd');
const fs = require('fs');


router.post('/', (request, response)=>{
    cmd.run(
        `
        cd ../../
        cd api_pro/${request.user.email}/${request.body.pname}/models
        touch ${request.body.table}.js
        `
    );

    var model_content = fs.readFileSync('/home/praveen/Documents/api_static/model.js', 'utf-8');
    var index_content = fs.readFileSync(`/home/praveen/Documents/api_pro/${request.user.email}/${request.body.pname}/index.js`);
    model_content = model_content.replace('Q_TABLE', request.body.table);
    model_content = model_content.replace('TABLE', request.body.table);
    model_content = model_content.replace('TABLE', request.body.table);
     // writing to index.js file
    fs.writeFile(`/home/praveen/Documents/api_pro/${request.user.email}/${request.body.pname}/models/${request.body.table}.js`, model_content , function (err) {
        if (err) throw err;
    })

    var import_stmt = `const ${request.body.table} = require('./models/${request.body.table}');\n` + index_content;
    fs.writeFile(`/home/praveen/Documents/api_pro/${request.user.email}/${request.body.pname}/index.js`, import_stmt , function (err) {
        if (err) throw err;
    })

    var resp_obj = {"message": "Model created success"};
    response.send(resp_obj);
    
});

module.exports = router;