const express = require('express');
const router = express.Router();
const fs = require('fs');


router.put('/:mname/schema', (request, response)=>{
    var schema_string = 'COL_NAME:{type:TYPE,allowNull:NULL,unique:UNIQUE},';
    var def_string = '';
    for(var i=0; i<request.body.columns.length; i++){
        var col_obj = request.body.columns[i];
        var temp = schema_string.replace('COL_NAME', col_obj.col_name).replace('TYPE', col_obj.type)
        .replace('NULL', col_obj.constraints.null).replace('UNIQUE', col_obj.constraints.unique);
        def_string = def_string.concat(temp);
    }
    def_string = '{' + def_string + '}';
    var model_content = fs.readFileSync(`/home/praveen/Documents/api_pro/${request.user.email}/${request.body.pname}/models/${request.params.mname}.js`, 'utf-8');
    var fi = model_content.indexOf('{');
    var li = model_content.lastIndexOf('}');
    var substr = model_content.substring(fi, li+1);
    model_content = model_content.replace(substr, def_string);
    fs.writeFile(`/home/praveen/Documents/api_pro/${request.user.email}/${request.body.pname}/models/${request.params.mname}.js`, model_content, function (err) {
        if (err) throw err;
        console.log('Updated model.js file!');
    });
    response.send("he"); 
});

module.exports = router;