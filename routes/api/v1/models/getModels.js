const express = require('express');
const router = express.Router();
const cmd = require('node-cmd');


router.get('/', (request, response)=>{
    cmd.get(
        `
        cd ../../
        cd api_pro/${request.user.email}/${request.body.pname}/models
        ls
        `, function(err, data, stderr){
            var file_array = data.split('\n');
            
            // contains all model files
            file_array = file_array.slice(0, file_array.length-1)

            // removing .js file extension 
            for(var i=0; i<file_array.length; i++){
                file_array[i] = file_array[i].slice(0, (file_array[i].length-3));
            }

            var resp_obj = {
                "project_name": request.body.pname,
                "models": file_array
            }
            response.send(resp_obj);
        }
    );
});

module.exports = router;