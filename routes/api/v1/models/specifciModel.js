const express = require('express');
const router = express.Router();
const fs = require('fs');


router.get('/:mname/schema', (request, response)=>{
    var content = fs.readFileSync(`/home/praveen/Documents/api_pro/${request.user.email}/${request.body.pname}/models/${request.params.mname}.js`,'utf-8');
    var fi = content.indexOf('{');
    var ei = content.lastIndexOf('}');
    var s = content.substring(fi+1,ei+1);
    var split_contents = s.split('},');
    var all_columns = [];
    for(var i=0;i<split_contents.length-1; i++){
        var col_obj = {};
        var substr = split_contents[i];
        var specific_split = substr.split(',');
        var first_split = specific_split[0];
        var col_name = first_split.substring(0, first_split.indexOf(':'));
        var col_type = first_split.substring(first_split.lastIndexOf(':')+1, first_split.length);
        var second_split = specific_split[1];
        var allownull = second_split.substring(second_split.indexOf(':')+1, second_split.length);
        var nullResult = allownull == "false" ? false: true;
        var third_split = specific_split[2];
        var allowUnique = third_split.substring(third_split.indexOf(':')+1, third_split.length);
        var uniqueResult = allowUnique == "false" ? false: true;
        col_obj.col_name = col_name;
        col_obj.type = col_type;
        var constraints = {};
        constraints.null = nullResult;
        constraints.unique = uniqueResult;
        col_obj.constraints = constraints;
        all_columns.push(col_obj);
    }
    var result = {};
    result.columns = all_columns;
    response.send(result);
})

module.exports = router;