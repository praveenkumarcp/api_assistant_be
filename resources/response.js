const RES = {
    account_created: {
        "message": "Account created successfully"
    },
    account_not_created: {
        "message": "Error in creating account"
    },
    invalid_login: {
        "message": "Invalid email or password"
    },
    valid_login: {
        "message": "Login success"
    },
    project_created: {
        "message": "Project created !"
    },
    project_not_created: {
        "message": "Error in creating project !"
    },
    max_no_project: {
        "message": "Max no of projects created !"
    },
    projects_fetced:{
        "message": "Project list fetched !"
    }
}

module.exports = RES;