const express = require('express');
const app = express();
var bodyParser = require('body-parser');

// custom imports
const sequelize = require('./config/db');
const CONFIG = require('./config/config');
const accounts = require('./routes/api/v1/accounts/accounts');
const projects = require('./routes/api/v1/projects/projects');
const models = require('./routes/api/v1/models/models');
const attachUserObject = require('./utils/utils').attachUserObject;

// checking DB connection
sequelize.authenticate()
.then(async()=>{
    console.log("DB connection success!");
    
    // syncing tables
    await sequelize.sync({force: false})
    .then(()=>console.log("DB sync success"))
    .catch((err)=>console.log(`error during DB sync ${err}`));

    // starting server
    app.listen(CONFIG.server_port,()=> console.log(`server listening on port ${CONFIG.server_port}`));
})
.catch((err) => {
    console.log(`error in connecting database ${err}`);
})

// middleware
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "*")
    res.header("Access-Control-Allow-Headers", "Origin, Authorization, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(attachUserObject);
// routes
app.use('/api/v1/accounts/', accounts);
app.use('/api/v1/be_projects/', projects);
app.use('/api/v1/projects/', models);
