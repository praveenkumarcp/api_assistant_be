

const sendResponse = (response, body, status)=>{
    response.status(status).send(body);
}

module.exports = sendResponse;